extension StringExt on String {
  String minLines(int count) => this + ("\n" * count);
}
